### Preview

![gif](https://i.postimg.cc/ydPyVfy4/dwm.gif "gif")

### Build and install
#### Requirements
git, xlib, xsetroot
###### ArchLinux:
- sudo pacman -Syu base-devel libx11 libxft libxinerama xorg-xsetroot git
###### Gentoo:
- doas emerge -av x11-libs/libX11 x11-libs/libXft x11-libs/libXinerama x11-apps/xsetroot dev-vcs/git
###### Debian:
- sudo apt-get install build-essential libx11-dev libxft-dev libxinerama-dev x11-xserver-utils git
###### Void:
- sudo xbps-install libX11-devel libXft-devel libXinerama-devel xsetroot

#### Actual install

- git clone https://gitlab.com/rmnsa/dwm.git
- cd dwm
- sudo make install clean
- !! add **exec dwm** to .xinitrc !!

### Keybinds
Super = windows key

|function|key1|key2|key3|
|:------:|:--:|:--:|:--:|
|open terminal|Super|Enter|
|open dmenu|Super|p|
|close window|Super|Shift|c|
|focus window to left|Super|j|
|focus window to right|Super|k|
|move window in stack down|Super|Shift|j|
|move window in stack up|Super|Shift|k|
|increase masters|Super|i|
|decrease masters|Super|d|
|swap (zoom) window|Super|Enter|
|view occupied tags|Super|Tab|
|tiling layout|Super|t|
|floating layout|Super|f|
|monocle layout|Super|m|
|set default layout|Super|Space|
|focus monitor to left|Super|comma|
|focus monitor to right|Super|period|
|move window to left monitor|Super|Shift|comma|
|move window to right monitor|Super|Shift|period|
|shrink gaps|Super|minus|
|expand gaps|Super|equal|
|reset gaps|Super|Shift|minus|
|toggle gaps|Super|Shift|equal|

###### Only in floating layout

|function in floating layout|key1|key2|key3|key4|
|:------:|:--:|:--:|:--:|:--:|
|move window left|Super|a|
|move window right|Super|d|
|move window up|Super|w|
|move window down|Super|s|
|resize window left|Super|Shift|a|
|resize window right|Super|Shift|d|
|resize window up|Super|Shift|w|
|resize window down|Super|Shift|s|
|resize window to the left|Super|Control|Shift|a|
|resize window to the right|Super|Control|Shift|d|
|resize window to the top|Super|Control|Shift|w|
|resize window to the bottom|Super|Control|Shift|s|

###### Other functionality

Windows can be "swallowed" by other windows.\
for example if you open firefox inside terminal, with\
this command **dwmswallow $WINDOWID ; firefox**,\
browser will replace current terminal window\
instead of spawning another window.

![gif2](https://i.postimg.cc/2SYkR2y7/dwms.gif "gif2")

### Configuration
configuration is done by editing config.h / config.def.h file and recompiling\
colorscheme, font, and basic configuration can be done by editing .Xresources\
in the following ways:\
	dwm.font:       monospace-10\
	dwm.selfgcolor: #ffffff\
also if you have entries like *.font, dwm should automaticaly apply given argument\
\
\
\
\
!! this is not my project, its only modified version of !!\
!! dynamic window manager from [suckless.org](https://dwm.suckless.org) !!\
!! and some [patches](https://dwm.suckless.org/patches/) from the same website !!
